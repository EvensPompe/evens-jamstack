# fullstack - JAMstack SD² challenge

## Objectifs : Green IT

Démontrer la possibilité de réduire le recours aux instances de compute pour propulser un site web.

#### Anti-pattern : Consommation en énergie et coûts important

Un wordpress ou un Drupal qui tourne en 24/7 365 jours par an sur la stack LAMP d'une ressource de compute et alors que les contenus restent inchangés. Recalcul systématique des page "on query". Base de données toujours disponible avec du stockage haute performance.

******

#### Pattern : Consommation en énergie à la demande et coûts optimisés

La JAM Stack : propulser un maximum d'assets statiques via du storage. Recalcul à la modification des contenus.  
L'adaptive infrastructure : mobiliser la ressource d'infrastructure quand elle est nécessaire, puis l'éteindre.  

## Le socle technologique

- Cloud provider (AWS, GCP, Azure, ...) ou on-premise (en local sur votre PC par exemple) :
- CMS (Content Management System) : Pour modifier le contenu de l'application web.
- Gitlab : dépôt de code, pipeline CI/CD, stockage des credentials du provider cloud.
- Terraform : Pour déployer/détruire plus rapidement l'infrastructure cloud.
- Ansible : Pour automatiser l'installation du CMS.
- React/Next : Pour développer l'application web statique. 

## Exemple de Scénario

1/ Déployer l'infrastructure cloud (Exemple sur AWS : VPC, subnet, route table, internet gateway, instance EC2).  
Installer le CMS (par exemple Strapi) sur l'instance EC2.  
2/ Développer une application web statique (par exemple en React/Next) qui, au moment du build, consomme l'API de strapi pour générer son contenu et est ensuite déployée sur un bucket S3 accessible sur Internet.  
3/ Développer un outil permettant de rallumer l'instance EC2 hébergeant le CMS lorsqu'elle est éteinte (par exemple une page HTML avec un simple bouton qui déclenche un appel API sur le service AWS "API Gateway" déclenchant une fonction écrite en python sur le service AWS "Lambda" qui rallume une instance EC2 éteinte).  

###### Application Web Statique = Single Page Application (SPA) :

Développée en React/Next ou Nuxt/Vue, elle présente un catalogue produit de votre choix (Un blog, un album, une description, un prix par produit, ...).  

Le build and deploy de la SPA est déclenché via Strapi lorsque le contenu est modifié (Exemple modification d'une image sur Strapi = webhook qui déclenche le pipeline CI/CD build/deploy sur GitLab).  
Le stage deploy publie la SPA sur un service de stockage d'objets (object storage : Amazon S3, GCP Cloud Storage, Azure storage account, ...) exposant la SPA sur Internet.  

###### Strapi :

Expose les entités du catalogue produits via API.  
Permet l'ajout et la modifications des produits du catalogue.  
Permet de publier via webhook en déclenchant la pipeline Gitlab ou Github du repo de la SPA.  

## Par où commencer

###### Créez un compte AWS/GCP/Azure.

###### Créez 3 repository GitLab : 

1/ Infrastructure  
2/ Frontend  
3/ Back Office HTML  

###### Repository Infrastructure :
Dépôt du code des scripts Terraform et Ansible (infra as code) et déploiement automatisé par le pipeline CI/CD GitLab.  

###### Repository Frontend :
Dépôt du code de l'application web statique et automatisation du build/deploy par le pipeline CI/CD GitLab.  

###### Repository Back Office HTML :
Dépôt du code de l'application permettant de rallumer le CMS (qui s'éteint manuellement ou après 15 minutes d'inactivité).  

## Condition de validation du challenge :

Etat initial : une version de la SPA est en ligne, hébergée sur un bucket (S3 ou autre).  Aucune ressource de compute n'est démarrée au sein de l'environnement Cloud.  
Le user démarre Strapi et les dépendances nécessaires (bdd, runner).  
Le user modifie le contenu du catalogue produit et publie.  
La pipeline de la SPA déploie la nouvelle version du catalogue sur le bucket.  
Le user éteint Strapi.  
Nouvelle itération...  
