# Description du repository  
   
Ce dossier est un exemple de ce à quoi pourrait ressembler votre repository Frontend.  
  
Pour développer la Single Page Application qui consommera l'API de Strapi au moment du build, vous pouvez vous inspirer du lien suivant : https://strapi.io/blog/build-a-blog-with-next-react-js-strapi  
  
N'oubliez pas de générer dans le service IAM une ACCESS_KEY et SECRET_KEY.  
  
Dans Settings > CI/CD > Variables, enregistrez 4 variables utilisées par la CI :  
- AWS_ACCESS_KEY_ID (valeur: générée sur IAM)  
- AWS_SECRET_ACCESS_KEY (valeur: générée sur IAM)  
- S3_BUCKET (valeur: URL du bucket)  
- AWS_DEFAULT_REGION (valeur: eu-west-3 pour la région de Paris)  
   
## .gitlab-ci.yml
  
Contient la CI permettant de build la Single Page Application et de la déployer dans un bucket S3.  
  