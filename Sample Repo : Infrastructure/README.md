# Description du repository  
  
Ce dossier est un exemple de ce à quoi pourrait ressembler votre repository Infrastructure.  
  
## Dossier Terraform
  
Contient des liens vers la documentation Terraform permettant de déployer l'infrastructure chez un cloud provider (AWS pour cet exemple).  
  
La première chose à faire est de créer un user dans le service IAM, de lui donner des droits, et copier/coller l'access key et secret key pour plus tard.   
  
Dans les faits, il existe déjà des ressources par défaut dans toutes les régions sur votre compte comme :  
  
- VPC  
- subnets  
- routes (Table de routage pour router le traffic dans le subnet + route par défaut vers Internet)  
- igw (Internet Gateway pour sortir vers Internet)  
  
Si vous ne connaissez pas/peu Terraform, contentez-vous de compléter les fichiers ec2.tf, provider.tf et security_group.tf  
  
Comme dit précédemment, il existe déjà dans la région de Paris sur votre compte AWS un VPC, 3 subnets, une table de routage attachée au VPC pour router le traffic entre les 3 subnets et une route par défaut vers l'Internet Gateway pour sortir vers Internet.  
  
Pour les services API Gateway et Lambda vous pouvez le faire manuellement sur l'interface web d'AWS (Pour rallumer l'instance EC2 hébergeant Strapi).  
  
## Dossier Ansible
  
Contient les fichiers permettant l'installation automatisée du CMS Strapi sur une instance (instance EC2 par exemple).  
  
## .gitlab-ci.yml
  
Contient une partie de la CI à compléter.  
